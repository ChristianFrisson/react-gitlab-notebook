'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _latest = require('axios-gitlab/dist/latest');

var _latest2 = _interopRequireDefault(_latest);

var _reactOauthPopup = require('react-oauth-popup');

var _reactOauthPopup2 = _interopRequireDefault(_reactOauthPopup);

var _reactSimpleStorage = require('react-simple-storage');

var _reactSimpleStorage2 = _interopRequireDefault(_reactSimpleStorage);

var _reactjsPopup = require('reactjs-popup');

var _reactjsPopup2 = _interopRequireDefault(_reactjsPopup);

var _formik = require('formik');

var _yup = require('yup');

var Yup = _interopRequireWildcard(_yup);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// import styles from './styles.css'

var ReactGitlabNotebook = function (_React$Component) {
    _inherits(ReactGitlabNotebook, _React$Component);

    function ReactGitlabNotebook(props) {
        _classCallCheck(this, ReactGitlabNotebook);

        var _this = _possibleConstructorReturn(this, (ReactGitlabNotebook.__proto__ || Object.getPrototypeOf(ReactGitlabNotebook)).call(this, props));

        _this.state = {
            redirectUri: '',
            gitlabUser: '',
            gitlabAvatarUrl: '',
            gitlabToken: '',
            api: '',
            gitlabProject: '',
            gitlabProjectId: '',
            gitlabProjectIssues: ''
        };
        return _this;
    }

    _createClass(ReactGitlabNotebook, [{
        key: 'onCode',
        value: function onCode(token) {
            var _this2 = this;

            var _props = this.props,
                gitlabUrl = _props.gitlabUrl,
                gitlabApplicationId = _props.gitlabApplicationId,
                gitlabProject = _props.gitlabProject;
            var _state = this.state,
                gitlabUser = _state.gitlabUser,
                gitlabAvatarUrl = _state.gitlabAvatarUrl,
                gitlabToken = _state.gitlabToken,
                gitlabProjectId = _state.gitlabProjectId,
                gitlabProjectIssues = _state.gitlabProjectIssues;

            var api = new _latest2.default({
                url: this.props.gitlabUrl,
                oauthToken: token
            });
            this.setState({
                gitlabToken: token,
                api: api
            });

            if (gitlabUser === '' || gitlabAvatarUrl === '') {
                api.Users.current().then(function (user) {
                    _this2.setState({
                        gitlabUser: user.name,
                        gitlabAvatarUrl: user.avatar_url
                    });
                }).catch(function (error) {
                    console.log('Error getting current user', error);
                });
            }

            if (gitlabProject !== '' && gitlabProjectId === '') {
                api.Projects.search(gitlabProject).then(function (projects) {
                    if (projects.length === 1) {
                        _this2.setState({
                            gitlabProjectId: projects[0].id
                        });
                    }
                }).catch(function (error) {
                    console.log('Error getting current project', error);
                });
            }
        }
    }, {
        key: 'onParentStateHydrated',
        value: function onParentStateHydrated() {
            var _state2 = this.state,
                gitlabUser = _state2.gitlabUser,
                gitlabAvatarUrl = _state2.gitlabAvatarUrl,
                gitlabToken = _state2.gitlabToken,
                api = _state2.api;

            if (api === '' && gitlabToken !== '') {
                this.onCode.bind(this)(gitlabToken);
            }
        }
    }, {
        key: 'logOut',
        value: function logOut() {
            this.setState({
                gitlabUrl: '',
                gitlabToken: '',
                gitlabUser: '',
                gitlabAvatarUrl: '',
                api: '',
                gitlabProjectId: '',
                gitlabProjectIssues: ''
            });
        }
    }, {
        key: 'addIssue',
        value: function addIssue(values) {
            var _this3 = this;

            var _props2 = this.props,
                gitlabUrl = _props2.gitlabUrl,
                gitlabApplicationId = _props2.gitlabApplicationId,
                gitlabProject = _props2.gitlabProject;
            var _state3 = this.state,
                api = _state3.api,
                gitlabUser = _state3.gitlabUser,
                gitlabAvatarUrl = _state3.gitlabAvatarUrl,
                gitlabToken = _state3.gitlabToken,
                gitlabProjectId = _state3.gitlabProjectId,
                gitlabProjectIssues = _state3.gitlabProjectIssues;

            console.log('addIssue');
            if (api !== '' && gitlabProjectId != '') {
                api.Issues.create(gitlabProjectId, {
                    title: values.title,
                    description: values.description
                }).then(function (result) {
                    console.log('Result', result);
                    _this3.setState({
                        gitlabProjectIssues: ''
                    });
                }).catch(function (error) {
                    console.log('Error adding issue', error);
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this4 = this;

            var _props3 = this.props,
                gitlabUrl = _props3.gitlabUrl,
                gitlabApplicationId = _props3.gitlabApplicationId,
                gitlabProject = _props3.gitlabProject,
                idyll = _props3.idyll,
                hasError = _props3.hasError,
                updateProps = _props3.updateProps,
                className = _props3.className,
                children = _props3.children,
                style = _props3.style,
                props = _objectWithoutProperties(_props3, ['gitlabUrl', 'gitlabApplicationId', 'gitlabProject', 'idyll', 'hasError', 'updateProps', 'className', 'children', 'style']);

            var _state4 = this.state,
                gitlabUser = _state4.gitlabUser,
                gitlabAvatarUrl = _state4.gitlabAvatarUrl,
                gitlabToken = _state4.gitlabToken,
                api = _state4.api,
                gitlabProjectId = _state4.gitlabProjectId,
                gitlabProjectIssues = _state4.gitlabProjectIssues;

            var redirectUri = this.state.redirectUri;
            if (redirectUri === '' && typeof window !== 'undefined') {
                redirectUri = window.location;
                redirectUri = redirectUri.toString().split('#')[0];
            }
            var login = '';
            var loginTitle = void 0;
            if (gitlabToken === '') {
                loginTitle = 'Log in with GitLab';
                login = _react2.default.createElement(
                    _reactOauthPopup2.default,
                    {
                        className: 'button',
                        url: gitlabUrl + '/oauth/authorize?client_id=' + gitlabApplicationId + '&redirect_uri=' + redirectUri + '&state=1&response_type=token&scope=api',
                        onCode: this.onCode.bind(this)
                    },
                    _react2.default.createElement(
                        'button',
                        null,
                        loginTitle
                    )
                );
            } else {
                loginTitle = 'Log out';
                login = _react2.default.createElement(
                    'button',
                    { onClick: this.logOut.bind(this) },
                    loginTitle
                );
            }
            var user = '';
            var avatar = '';
            if (gitlabUser !== '') {
                user = _react2.default.createElement(
                    'div',
                    { className: 'user title' },
                    gitlabUser
                );
            }
            if (gitlabAvatarUrl !== '') {
                avatar = _react2.default.createElement(
                    'div',
                    { className: 'user avatar' },
                    _react2.default.createElement('img', { alt: gitlabUser, src: gitlabAvatarUrl })
                );
            }
            var issuesList = '';
            if (api !== '' && gitlabProjectId !== '' && gitlabProjectIssues === '') {
                // console.log('api',api);
                api.Issues.all({
                    projectId: gitlabProjectId,
                    state: "opened"
                }).then(function (issues) {
                    //console.log('issues', issues);
                    _this4.setState({
                        gitlabProjectIssues: issues
                    });
                }).catch(function (error) {
                    console.log('Error getting current project issues', error);
                });
            }
            var self = this;
            if (gitlabProjectIssues !== '') {
                issuesList = _react2.default.createElement(
                    'div',
                    { className: 'issues' },
                    _react2.default.createElement(
                        'div',
                        { className: 'title' },
                        'Issues'
                    ),
                    _react2.default.createElement(
                        _reactjsPopup2.default,
                        { trigger: _react2.default.createElement(
                                'button',
                                null,
                                ' Add'
                            ), position: 'right center' },
                        function (closePopup) {
                            return _react2.default.createElement(
                                'div',
                                null,
                                _react2.default.createElement(
                                    _formik.Formik,
                                    {
                                        initialValues: { title: '', description: '' },
                                        validationSchema: Yup.object().shape({
                                            title: Yup.string().required('Required'),
                                            description: Yup.string()
                                        }),
                                        onSubmit: function onSubmit(values, actions) {
                                            self.addIssue.bind(self)(values);
                                            closePopup();
                                        },
                                        onReset: closePopup
                                    },
                                    function (props) {
                                        var values = props.values,
                                            touched = props.touched,
                                            errors = props.errors,
                                            dirty = props.dirty,
                                            isSubmitting = props.isSubmitting,
                                            handleChange = props.handleChange,
                                            handleSubmit = props.handleSubmit,
                                            handleBlur = props.handleBlur,
                                            handleReset = props.handleReset;

                                        return _react2.default.createElement(
                                            'form',
                                            { onSubmit: handleSubmit },
                                            _react2.default.createElement(
                                                'label',
                                                { htmlFor: 'title', style: { display: 'block' } },
                                                'Title'
                                            ),
                                            _react2.default.createElement('input', {
                                                id: 'title',
                                                placeholder: 'Enter the title of the issue',
                                                type: 'text',
                                                value: values.title,
                                                onChange: handleChange,
                                                onBlur: handleBlur,
                                                className: errors.title && touched.title ? 'text-input error' : 'text-input'
                                            }),
                                            _react2.default.createElement(
                                                'label',
                                                { htmlFor: 'description', style: { display: 'block' } },
                                                'Description'
                                            ),
                                            _react2.default.createElement('input', {
                                                id: 'description',
                                                placeholder: 'Enter the description of the issue',
                                                type: 'text',
                                                value: values.description,
                                                onChange: handleChange,
                                                onBlur: handleBlur,
                                                className: errors.description && touched.description ? 'text-input error' : 'text-input'
                                            }),
                                            errors.description && touched.description && _react2.default.createElement(
                                                'div',
                                                { className: 'input-feedback' },
                                                errors.description
                                            ),
                                            _react2.default.createElement(
                                                'button',
                                                {
                                                    type: 'button',
                                                    className: 'outline',
                                                    onClick: handleReset,
                                                    disabled: isSubmitting
                                                },
                                                'Cancel'
                                            ),
                                            _react2.default.createElement(
                                                'button',
                                                {
                                                    type: 'submit',
                                                    disabled: !dirty || isSubmitting
                                                },
                                                'Add'
                                            )
                                        );
                                    }
                                )
                            );
                        }
                    ),
                    gitlabProjectIssues.map(function (issue, ii) {
                        return _react2.default.createElement(
                            'div',
                            { key: ii, className: 'issue' },
                            _react2.default.createElement(
                                'div',
                                { className: 'title' },
                                _react2.default.createElement(
                                    'a',
                                    { href: issue.web_url },
                                    issue.title
                                )
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'id' },
                                '#',
                                issue.iid
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'author avatar' },
                                _react2.default.createElement(
                                    'a',
                                    { href: issue.author.web_url },
                                    _react2.default.createElement('img', { alt: issue.author.name, src: issue.author.avatar_url })
                                )
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'to' },
                                '\u2192'
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'assignee avatar' },
                                issue.assignee ? _react2.default.createElement(
                                    'a',
                                    { href: issue.assignee.web_url },
                                    _react2.default.createElement('img', { alt: issue.assignee.name, src: issue.assignee.avatar_url })
                                ) : _react2.default.createElement(
                                    'div',
                                    null,
                                    '?'
                                )
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'labels' },
                                issue.labels.map(function (label, li) {
                                    return _react2.default.createElement(
                                        'div',
                                        { key: li, className: 'label' },
                                        '(',
                                        label,
                                        ')'
                                    );
                                })
                            )
                        );
                    })
                );
            }
            return _react2.default.createElement(
                'div',
                { className: 'login' },
                _react2.default.createElement(_reactSimpleStorage2.default, {
                    parent: this,
                    blacklist: ['redirectUri', 'api', 'gitlabProjectId', 'gitlabProjectIssues'],
                    onParentStateHydrated: this.onParentStateHydrated.bind(this)
                }),
                _react2.default.createElement(
                    'div',
                    { className: 'title' },
                    'Reviewer: '
                ),
                user,
                avatar,
                login,
                issuesList
            );
        }
    }]);

    return ReactGitlabNotebook;
}(_react2.default.Component);

ReactGitlabNotebook.defaultProps = {
    gitlabUrl: 'https://gitlab.com',
    gitlabApplicationId: '', // Application Id See: https://docs.gitlab.com/ee/integration/oauth_provider.html
    gitlabProject: '' // Gitlab repository project name
};

exports.default = ReactGitlabNotebook;