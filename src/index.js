import React from 'react'
import PropTypes from 'prop-types'
// import styles from './styles.css'

import Gitlab from 'axios-gitlab/dist/latest';
import OauthPopup from 'react-oauth-popup';
import SimpleStorage from 'react-simple-storage';
import Popup from "reactjs-popup";
import { Formik } from 'formik';
import * as Yup from 'yup';

class ReactGitlabNotebook extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectUri: '',
            gitlabUser: '',
            gitlabAvatarUrl: '',
            gitlabToken: '',
            api: '',
            gitlabProject: '',
            gitlabProjectId: '',
            gitlabProjectIssues: ''
        };
    }

    onCode(token) {
        const { gitlabUrl, gitlabApplicationId, gitlabProject } = this.props;
        const { gitlabUser, gitlabAvatarUrl, gitlabToken, gitlabProjectId, gitlabProjectIssues } = this.state;
        const api = new Gitlab({
            url: this.props.gitlabUrl,
            oauthToken: token
        })
        this.setState({
            gitlabToken: token,
            api: api
        });

        if (gitlabUser === '' || gitlabAvatarUrl === '') {
            api.Users.current()
                .then((user) => {
                    this.setState({
                        gitlabUser: user.name,
                        gitlabAvatarUrl: user.avatar_url
                    });
                })
                .catch((error) => {
                    console.log('Error getting current user', error);
                })
        }

        if (gitlabProject !== '' && gitlabProjectId === '') {
            api.Projects.search(gitlabProject)
                .then((projects) => {
                    if (projects.length === 1) {
                        this.setState({
                            gitlabProjectId: projects[0].id
                        });
                    }
                })
                .catch((error) => {
                    console.log('Error getting current project', error);
                })

        }
    };

    onParentStateHydrated() {
        const { gitlabUser, gitlabAvatarUrl, gitlabToken, api } = this.state;
        if (api === '' && gitlabToken !== '') {
            this.onCode.bind(this)(gitlabToken);
        }
    }

    logOut() {
        this.setState({
            gitlabUrl: '',
            gitlabToken: '',
            gitlabUser: '',
            gitlabAvatarUrl: '',
            api: '',
            gitlabProjectId: '',
            gitlabProjectIssues: ''
        });
    }

    addIssue(values) {
        const { gitlabUrl, gitlabApplicationId, gitlabProject } = this.props;
        const { api, gitlabUser, gitlabAvatarUrl, gitlabToken, gitlabProjectId, gitlabProjectIssues } = this.state;
        console.log('addIssue')
        if (api !== '' && gitlabProjectId != '')
        {
            api.Issues.create(gitlabProjectId,{
                title: values.title,
                description: values.description
            })
            .then((result) => {
                console.log('Result', result);
                this.setState({
                    gitlabProjectIssues: ''
                });

            })
            .catch((error) => {
                console.log('Error adding issue', error);
            })
        }
    }

    render() {
        const { gitlabUrl, gitlabApplicationId, gitlabProject, idyll, hasError, updateProps, className, children, style, ...props } = this.props;
        const { gitlabUser, gitlabAvatarUrl, gitlabToken, api, gitlabProjectId, gitlabProjectIssues } = this.state;
        let redirectUri = this.state.redirectUri;
        if (redirectUri === '' && typeof window !== 'undefined') {
            redirectUri = window.location;
            redirectUri = redirectUri.toString().split('#')[0];
        }
        let login = '';
        let loginTitle;
        if (gitlabToken === '') {
            loginTitle = 'Log in with GitLab';
            login = <OauthPopup
                className="button"
                url={`${gitlabUrl}/oauth/authorize?client_id=${gitlabApplicationId}&redirect_uri=${redirectUri}&state=1&response_type=token&scope=api`}
                onCode={this.onCode.bind(this)}
            >
                <button>{loginTitle}</button>
            </OauthPopup>;
        }
        else {
            loginTitle = 'Log out';
            login = <button onClick={this.logOut.bind(this)}>{loginTitle}</button>
        }
        let user = '';
        let avatar = '';
        if (gitlabUser !== '') {
            user = <div className="user title">{gitlabUser}</div>
        }
        if (gitlabAvatarUrl !== '') {
            avatar = <div className="user avatar"><img alt={gitlabUser} src={gitlabAvatarUrl} /></div>
        }
        let issuesList = '';
        if (api !== '' && gitlabProjectId !== '' && gitlabProjectIssues === '') {
            // console.log('api',api);
            api.Issues.all({
                projectId: gitlabProjectId,
                state: "opened"
            })
                .then((issues) => {
                    //console.log('issues', issues);
                    this.setState({
                        gitlabProjectIssues: issues
                    });
                })
                .catch((error) => {
                    console.log('Error getting current project issues', error);
                })
        }
        const self = this;
        if (gitlabProjectIssues !== '') {
            issuesList =
                <div className="issues">
                    <div className="title">Issues</div>
                    <Popup trigger={<button> Add</button>} position="right center">
                        {closePopup => (
                            <div>
                                <Formik
                                    initialValues={{ title: '', description: '' }}
                                    validationSchema={Yup.object().shape({
                                        title: Yup.string().required('Required'),
                                        description: Yup.string()
                                    })}
                                    onSubmit={
                                        (values, actions) => {
                                            self.addIssue.bind(self)(values)
                                            closePopup();
                                        }
                                    }
                                    onReset={closePopup}
                                >
                                    {props => {
                                        const {
                                            values,
                                            touched,
                                            errors,
                                            dirty,
                                            isSubmitting,
                                            handleChange,
                                            handleSubmit,
                                            handleBlur,
                                            handleReset,
                                        } = props;
                                        return (
                                            <form onSubmit={handleSubmit}>
                                                <label htmlFor="title" style={{ display: 'block' }}>Title</label>
                                                <input
                                                    id="title"
                                                    placeholder="Enter the title of the issue"
                                                    type="text"
                                                    value={values.title}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className={
                                                        errors.title && touched.title ? 'text-input error' : 'text-input'
                                                    }
                                                />
                                                <label htmlFor="description" style={{ display: 'block' }}>Description</label>
                                                <input
                                                    id="description"
                                                    placeholder="Enter the description of the issue"
                                                    type="text"
                                                    value={values.description}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className={
                                                        errors.description && touched.description ? 'text-input error' : 'text-input'
                                                    }
                                                />
                                                {errors.description && touched.description && <div className="input-feedback">{errors.description}</div>}
                                                <button
                                                    type="button"
                                                    className="outline"
                                                    onClick={handleReset}
                                                    disabled={isSubmitting}
                                                >Cancel</button>
                                                <button
                                                    type="submit"
                                                    disabled={!dirty || isSubmitting}
                                                >Add</button>
                                            </form>
                                        );
                                    }}
                                </Formik>
                            </div>
                        )}
                    </Popup>
                    {gitlabProjectIssues.map((issue, ii) => (
                        <div key={ii} className="issue">
                            <div className="title"><a href={issue.web_url}>{issue.title}</a></div>
                            <div className="id">#{issue.iid}</div>
                            <div className="author avatar">
                                <a href={issue.author.web_url}><img alt={issue.author.name} src={issue.author.avatar_url} /></a></div>
                            <div className="to">&rarr;</div>
                            <div className="assignee avatar">
                                {issue.assignee ?
                                    <a href={issue.assignee.web_url}><img alt={issue.assignee.name} src={issue.assignee.avatar_url} /></a>
                                    :
                                    <div>?</div>
                                }
                            </div>
                            <div className="labels">
                                {issue.labels.map((label, li) => (
                                    <div key={li} className="label">({label})</div>
                                ))}
                            </div>
                        </div>
                    ))}
                </div>
        }
        return (
            <div className="login">
                <SimpleStorage
                    parent={this}
                    blacklist={['redirectUri', 'api', 'gitlabProjectId', 'gitlabProjectIssues']}
                    onParentStateHydrated={this.onParentStateHydrated.bind(this)}
                />
                <div className="title">Reviewer: </div>
                {user}
                {avatar}
                {login}
                {issuesList}
            </div>
        );
    }
}

ReactGitlabNotebook.defaultProps = {
    gitlabUrl: 'https://gitlab.com',
    gitlabApplicationId: '', // Application Id See: https://docs.gitlab.com/ee/integration/oauth_provider.html
    gitlabProject: '' // Gitlab repository project name
};

export default ReactGitlabNotebook;
